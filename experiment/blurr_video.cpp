#include "cv.h"
#include "highgui.h"

void example2_4( CvCapture* capture ){
	// Create some windows to show the input
	// and output images in.
	//
	
	assert( capture != NULL );

	cvNamedWindow("Example4-in");
	cvNamedWindow("Example4-out");
	
	IplImage* frame;

	while(1) {
		frame = cvQueryFrame( capture );
		if(!frame) break;
		
		cvShowImage("Example4-in", frame);
		cvSmooth(frame, frame, CV_GAUSSIAN, 9, 9);
		cvShowImage("Example4-out", frame);
		char c = cvWaitKey(1);
		if( c == 27 ) break;
	}
	
	cvDestroyWindow("Example4-in");
	cvDestroyWindow("Example4-out");
}

int main (int argc, char** argv){
	//example2_4(cvCreateFileCapture(argv[1]));
	CvCapture* cam = cvCaptureFromCAM(CV_CAP_ANY);
	example2_4(cam);
	return 0;
}
