#include "highgui.h"
#include "iostream"

using namespace std;

int main( int argc, char** argv ) {
	cvNamedWindow("Example2", CV_WINDOW_AUTOSIZE );
	//CvCapture* capture = cvCreateFileCapture("/media/sda3/Video/The.Big.Bang.Theory.S06E15.iTALiAN.SUBBED.HDTV.XviD-GaNNiCO.avi");
	CvCapture* capture = cvCaptureFromCAM(CV_CAP_ANY);
	IplImage* frame;
	while(1) {
		frame = cvQueryFrame( capture );
		//cout << frame->width << " " << frame->height << endl;
		if( !frame ) break;
		cvShowImage("Example2", frame );
		char c = cvWaitKey(33);
		if( c == 27 ) break;
	}
	cvReleaseCapture( &capture );
	cvDestroyWindow("Example2");
}

