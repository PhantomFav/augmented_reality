#include "opencv2/opencv.hpp"

using namespace cv;

int main(int, char**)
{
    VideoCapture webcam(0); // open the default camera
    if(!webcam.isOpened())  // check if we succeeded
        return -1;

    Mat edges;
    namedWindow("aug",1);
    for(;;){
        Mat frame;
        webcam >> frame; // get a new frame from camera
        cvtColor(frame, edges, CV_BGR2GRAY);
        GaussianBlur(edges, edges, Size(7,7), 1.5, 1.5);
        Canny(edges, edges, 0, 30, 3);
        imshow("aug", edges);
        if(waitKey(30) >= 0) break;
    }
    // the camera will be deinitialized automatically in VideoCapture destructor
    return 0;
}
