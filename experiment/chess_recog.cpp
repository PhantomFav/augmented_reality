#include <opencv2/opencv.hpp>
#include <iostream>
//g++ `pkg-config --cflags opencv` main.cpp augmented_reality.cpp `pkg-config --libs opencv` -lglut -lGL -lGLU -o programma

using namespace cv;
using namespace std;

int main0(int argc, char** argv)
{
    vector<Point2f> corners;
    float ref[] = {3,3, 6,3, 9,3, 12,3, 15,3, 18,3,
                 3,6, 6,6, 9,6, 12,6, 15,6, 18,6,
                 3,9, 6,9, 9,9, 12,9, 15,9, 18,9,
                 3,12, 6,12, 9,12, 12,12, 15,12, 18,12};
    vector<Point2f> reference;

    for(int i=0; i<48; i+=2){
        reference.push_back(Point2f(ref[i]*20.0,ref[i+1]*20.0));
    }


    Size patternsize(6,4);

    VideoCapture webcam(1); // open the default camera
    if(!webcam.isOpened())  // check if we succeeded
        return -1;

    Mat edges;
    namedWindow("aug",1);
    for(;;){
        Mat frame;
        Mat operation;
        Mat homography;
        Mat board;

        webcam >> frame; // get a new frame from camera
        //flip(frame, frame, 1);

        resize(frame, operation, Size(), 0.5, 0.5, CV_INTER_AREA);
        cvtColor(operation, operation, CV_RGB2GRAY);
        GaussianBlur(operation, operation, Size(7,7), 1.5, 1.5);

        bool patternfound = findChessboardCorners(operation, patternsize, corners,
                        CALIB_CB_ADAPTIVE_THRESH+CALIB_CB_NORMALIZE_IMAGE+CALIB_CB_FAST_CHECK);

        if(patternfound){
            cornerSubPix(operation, corners, Size(11, 11), Size(-1, -1),
                TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 30, 0.1));
            homography = findHomography(reference, corners, CV_RANSAC );
            cvtColor(operation, operation, CV_GRAY2RGBA);
            warpPerspective(operation, board, homography, operation.size());

            Rect roi(Point(corners[0].x, corners[0].y), board.size());
            cvtColor(frame, frame, CV_RGB2RGBA);
            //board.copyTo(frame(roi));
        }

        for(int i = 0; i<corners.size(); i++){
            corners[i].x*=2; corners[i].y*=2;
        }

        drawChessboardCorners(frame, patternsize, Mat(corners), patternfound);


        imshow("aug", frame);
        if(waitKey(30) >= 0) break;
    }
    // the camera will be deinitialized automatically in VideoCapture destructor
    return 0;
}
