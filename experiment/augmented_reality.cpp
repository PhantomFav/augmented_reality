#include <math.h>
#include <GL/glut.h>
#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

GLfloat angle = 0.0;
GLuint listIndex;
GLuint texture;
VideoCapture webcam(0);
Mat object;
Mat image;
Mat rvect;
Mat tvect;
Size patternsize(6,4);
Mat edges;
CvMat *in = (CvMat*)cvLoad("../AugmentedReality/Intrinsics.xml");
Mat intrinsic(in);
CvMat *dis = (CvMat*)cvLoad("../AugmentedReality/Distortion.xml");
Mat distortion(dis);

double ref[] = {0.0,0.0, 0.0,3.0, 0.0,6.0, 0.0,9.0, 0.0,12.0, 0.0,15.0,
                3.0,0.0, 3.0,3.0, 3.0,6.0, 3.0,9.0, 3.0,12.0, 3.0,15.0,
                6.0,0.0, 6.0,3.0, 6.0,6.0, 6.0,9.0, 6.0,12.0, 6.0,15.0,
                9.0,0.0, 9.0,3.0, 9.0,6.0, 9.0,9.0, 9.0,12.0, 9.0,15.0};

GLuint ConvertMatToTexture(Mat image)
{
    GLuint texture;
    
    glGenTextures(1, &texture);                  // Create The Texture
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S , GL_REPEAT);
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    glTexImage2D(GL_TEXTURE_2D, 0, 3, image.cols, image.rows, 0, GL_BGR, GL_UNSIGNED_BYTE, image.data);

    return texture;
}

GLvoid DrawVideo(){
    Mat frame;
    Mat operation;
    Mat rotm;
    GLfloat dirMat[16];

    webcam >> frame; // get a new frame from camera

    resize(frame, operation, Size(), 0.5, 0.5, CV_INTER_AREA);
    cvtColor(operation, operation, CV_RGB2GRAY);
    GaussianBlur(operation, operation, Size(7,7), 1.5, 1.5);

    bool patternfound = findChessboardCorners(operation, patternsize, image,
                    CALIB_CB_ADAPTIVE_THRESH+CALIB_CB_NORMALIZE_IMAGE+CALIB_CB_FAST_CHECK);

    if(patternfound){
        //cornerSubPix(operation, corners, Size(11, 11), Size(-1, -1),
        //    TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 30, 0.1));
        //homography = findHomography(corners, reference, CV_RANSAC );
        solvePnP(object, image*2, intrinsic, distortion, rvect, tvect);

        cout << tvect << endl;
        Rodrigues(rvect, rotm);
        //cout << rotm << endl;

        dirMat[0]=rotm.at<double>(0,0);  dirMat[4]=rotm.at<double>(0,1);  dirMat[8]=rotm.at<double>(0,2);   dirMat[12]=-tvect.at<double>(0)*380;
        dirMat[1]=rotm.at<double>(1,0);  dirMat[5]=rotm.at<double>(1,1);  dirMat[9]=rotm.at<double>(1,2);   dirMat[13]=-tvect.at<double>(1)*380;
        dirMat[2]=rotm.at<double>(2,0);  dirMat[6]=rotm.at<double>(2,1);  dirMat[10]=rotm.at<double>(2,2);  dirMat[14]=-tvect.at<double>(2)*380;
        dirMat[3]=0.0;                   dirMat[7]=0.0;                   dirMat[11]=0.0;                   dirMat[15]=1.0;
    }

    flip(frame, frame, -1);
    //drawChessboardCorners(frame, patternsize, Mat(image*2), patternfound);

    texture = ConvertMatToTexture(frame);

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture);
    glCallList(listIndex);
    glDisable(GL_TEXTURE_2D);

    if(patternfound){
    glPushMatrix();
        glMultMatrixf(dirMat);
        glTranslatef(0.0,0.0,-650.0);
        glDepthFunc(GL_ALWAYS);
            glBegin(GL_QUADS);
                glVertex3f(-1500.0, -3000.0, 0.0f);
                glVertex3f(1500.0, -3000.0, 0.0f);
                glVertex3f(1500.0, 3000.0, 0.0f);
                glVertex3f(-1500.0, 3000.0, 0.0f);
            glEnd();
        glDepthFunc(GL_LESS);
    glPopMatrix();
    }

}


void init(){
    glEnable(GL_DEPTH_TEST);
    glClearColor(0.0, 0.0, 0.0, 1.0);
}

void display()
{
    cout << "asd" << endl;
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    //gluLookAt(0.0, 0.0, 650.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
    DrawVideo();
    glutSwapBuffers();
}


void build_opengl_projection_for_intrinsics(Mat &frustum, int *viewport, double alpha, double beta, double skew, double u0, double v0, int img_width, int img_height, double near_clip, double far_clip){
    // These parameters define the final viewport that is rendered into by
    // the camera.
    cout << "asd" << endl;
    double L = 0;
    double R = img_width;
    double B = 0;
    double T = img_height;

    // near and far clipping planes, these only matter for the mapping from
    // world-space z-coordinate into the depth coordinate for OpenGL
    double N = near_clip;
    double F = far_clip;

    // set the viewport parameters
    viewport[0] = L;
    viewport[1] = B;
    viewport[2] = R-L;
    viewport[3] = T-B;

    // construct an orthographic matrix which maps from projected
    // coordinates to normalized device coordinates in the range
    // [-1, 1].  OpenGL then maps coordinates in NDC to the current
    // viewport

    Mat ortho = Mat::zeros(4, 4, CV_64F);
    //Eigen::Matrix4d ortho = Eigen::Matrix4d::Zero();
    ortho.at<double>(0,0) =  2.0/(R-L); ortho.at<double>(0,3) = -(R+L)/(R-L);
    ortho.at<double>(1,1) =  2.0/(T-B); ortho.at<double>(1,3) = -(T+B)/(T-B);
    ortho.at<double>(2,2) = -2.0/(F-N); ortho.at<double>(2,3) = -(F+N)/(F-N);
    ortho.at<double>(3,3) =  1.0;

    // construct a projection matrix, this is identical to the
    // projection matrix computed for the intrinsicx, except an
    // additional row is inserted to map the z-coordinate to
    // OpenGL.
    Mat tproj = Mat::zeros(4, 4, CV_64F);
    //Eigen::Matrix4d tproj = Eigen::Matrix4d::Zero();
    tproj.at<double>(0,0) = alpha; tproj.at<double>(0,1) = skew; tproj.at<double>(0,2) = u0;
                                   tproj.at<double>(1,1) = beta; tproj.at<double>(1,2) = v0;
                                                                 tproj.at<double>(2,2) = -(N+F); tproj.at<double>(2,3) = -N*F;
                                                                 tproj.at<double>(3,2) = 1.0;

    // resulting OpenGL frustum is the product of the orthographic
    // mapping to normalized device coordinates and the augmented
    // camera intrinsic matrix
    frustum = ortho*tproj;
}

void reshape(int w, int h){

    double projMat[16];

    glViewport(0, 0, (GLsizei)w, (GLsizei)h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    Mat frustum(4,4,DataType<double>::type);
    int viewport[4];
    cout << "asda" << endl;

    float asd;
    asd = intrinsic.at<float>(0,0);
    cout << asd << endl;
    asd = intrinsic.at<float>(1,1);
    cout << asd << endl;
    asd = intrinsic.at<float>(0,1);
    cout << asd << endl;
    asd = intrinsic.at<float>(0,2);
    cout << asd << endl;
    asd = intrinsic.at<float>(1,2);
    cout << asd << endl;

    build_opengl_projection_for_intrinsics(frustum, viewport, intrinsic.at<float>(0,0), intrinsic.at<float>(1,1), intrinsic.at<float>(0,1), intrinsic.at<float>(0,2), intrinsic.at<float>(1,2), 640, 480, 0.1, 50000.0);
    cout << "asd" << endl;
    cout << frustum << endl;

    cout << "asd" << endl;
    projMat[0]=frustum.at<double>(0,0);  projMat[4]=frustum.at<double>(0,1);  projMat[8]=frustum.at<double>(0,2);   projMat[12]=frustum.at<double>(0,3);
    projMat[1]=frustum.at<double>(1,0);  projMat[5]=frustum.at<double>(1,1);  projMat[9]=frustum.at<double>(1,2);   projMat[13]=frustum.at<double>(1,3);
    projMat[2]=frustum.at<double>(2,0);  projMat[6]=frustum.at<double>(2,1);  projMat[10]=frustum.at<double>(2,2);  projMat[14]=frustum.at<double>(2,3);
    projMat[3]=frustum.at<double>(3,0);  projMat[7]=frustum.at<double>(3,1);  projMat[11]=frustum.at<double>(3,2);  projMat[15]=frustum.at<double>(3,3);
    //glMultMatrixf(projMat);
    cout << "asd" << endl;
    glLoadMatrixd(&projMat[0]);
    cout << "asd" << endl;

    //gluPerspective(60, (GLfloat)w / (GLfloat)h, 1.0, 50000.0);
    //glFrustum(-320.0, 320.0, -240.0, 240.0, 600.0, 50000.0);
    //glOrtho(320, -320, 240, -240, 1.0, 50000.0);
    glMatrixMode(GL_MODELVIEW);


}


void keyboard (unsigned char key, int x, int y)
{
    switch(key){
    case 27: case 'q':
        exit (0);
        break;
    }
}


void initialize_opencv(){
    listIndex = glGenLists(1);
    glNewList(listIndex, GL_COMPILE);
        glPushMatrix();
        glTranslatef(0.0,0.0,-650.0);
        glBegin(GL_QUADS);
            glTexCoord2f (0.0, 0.0);
            glVertex3f(-320.0f, -240.0f, 0.0f);
            glTexCoord2f (1.0, 0.0);
            glVertex3f(320.0f,-240.0f, 0.0f);
            glTexCoord2f (1.0, 1.0);
            glVertex3f(320.0f,240.0f, 0.0f);
            glTexCoord2f (0.0, 1.0);
            glVertex3f(-320.0f, 240.0f, 0.0f);
        glEnd();
        glPopMatrix();
    glEndList();
}

int main(int argc, char **argv)
{

    glutInit(&argc, argv);
    glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
    glutInitWindowSize(640, 480); // 4/3
    glutInitWindowPosition(100, 100);
    glutCreateWindow("Cam");

    if(!webcam.isOpened())  // check if we succeeded
        return -1;

    for(int i=0; i<48; i+=2){
        object.push_back(Point3d(ref[i],ref[i+1],0.0));
    }

    glEnable(GL_CULL_FACE);
    initialize_opencv();
    init();
    glutDisplayFunc(display);
    glutKeyboardFunc(keyboard);
    glutIdleFunc(display);
    glutReshapeFunc(reshape);
    glutMainLoop();
}
