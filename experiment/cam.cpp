#include <math.h>
#include <GL/glut.h>
#include "highgui.h"

GLfloat angle = 0.0; 
GLuint listIndex;
GLuint texture;
CvCapture* capture;

GLuint ConvertIplToTexture(IplImage *image)
{
  GLuint texture;

  glGenTextures(1,&texture);
  glBindTexture(GL_TEXTURE_2D,texture);
  gluBuild2DMipmaps(GL_TEXTURE_2D,3,image->width,image->height,
  GL_BGR,GL_UNSIGNED_BYTE,image->imageData);

 return texture;
}

GLvoid DrawVideo()
{
	IplImage* frame;
	frame = cvQueryFrame( capture );
	texture = ConvertIplToTexture(frame);
	
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texture);
    glCallList(listIndex);
	glDisable(GL_TEXTURE_2D);
}


void init(){
	glEnable(GL_DEPTH_TEST);
	glClearColor(0.0, 0.0, 0.0, 1.0);
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	gluLookAt(0.0, 0.0, 2.60, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);	
	DrawVideo();
	glutSwapBuffers();

}

void reshape(int w, int h){
	glViewport(0, 0, (GLsizei)w, (GLsizei)h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60, (GLfloat)w / (GLfloat)h, 1.0, 100.0);
	glMatrixMode(GL_MODELVIEW);
}


void keyboard (unsigned char key, int x, int y)
{
	switch(key){
	case 27: case 'q':
		exit (0);
		break;
	}
}


void initialize_opencv(){
	listIndex = glGenLists(1);
    glNewList(listIndex, GL_COMPILE);
		glBegin(GL_QUADS);
		   	glTexCoord2f (0.0, 1.0);
		    glVertex3f(-2.0f, -1.5f, 0.0f);
		   	glTexCoord2f (1.0, 1.0);
		    glVertex3f(2.0f, -1.5f, 0.0f);
		   	glTexCoord2f (1.0, 0.0);
		    glVertex3f(2.0f,1.5f, 0.0f);
		   	glTexCoord2f (0.0, 0.0);
		    glVertex3f(-2.0f,1.5f, 0.0f);
	    glEnd();  
	glEndList();
}

int main(int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowSize(640, 480); // 4/3
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Cam");

	//capture = cvCreateFileCapture("/media/sda3/Video/The.Big.Bang.Theory.S06E15.iTALiAN.SUBBED.HDTV.XviD-GaNNiCO.avi");
	capture = cvCaptureFromCAM(CV_CAP_ANY);

	glEnable(GL_CULL_FACE);
	initialize_opencv();
	init();
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutIdleFunc(display);
	glutReshapeFunc(reshape);
	glutMainLoop();
}
