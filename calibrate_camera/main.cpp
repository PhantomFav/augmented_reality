// calib.cpp
// Calling convention:
// calib board_w board_h number_of_views
//
// Hit ‘p’ to pause/unpause, ESC to quit
//
#include <iostream>
#include <opencv2/opencv.hpp>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int n_boards = 0; //Will be set by input list
const int board_dt = 20; //Wait 20 frames per chessboard view
int board_w;
int board_h;

using namespace std;
using namespace cv;

int main(int argc, char* argv[]) {
		
	board_w = 12;
	board_h = 8;
	n_boards = 12;
	int board_n = board_w * board_h;
	CvSize board_sz = cvSize( board_w, board_h );
    CvCapture* capture = cvCreateCameraCapture(0);
	assert( capture );
	cvNamedWindow("Calibration");

	//ALLOCATE STORAGE
	CvMat* image_points = cvCreateMat(n_boards*board_n,2,CV_32FC1);
	CvMat* object_points = cvCreateMat(n_boards*board_n,3,CV_32FC1);
	CvMat* point_counts = cvCreateMat(n_boards,1,CV_32SC1);
	CvMat* intrinsic_matrix = cvCreateMat(3,3,CV_32FC1);
	CvMat* distortion_coeffs = cvCreateMat(5,1,CV_32FC1);
	CvPoint2D32f* corners = new CvPoint2D32f[ board_n ];
	
	int corner_count;
	int successes = 0;
	int step, frame = 0;
	string path = "../data/imagel";

	//IplImage *image = cvQueryFrame( capture );

	// CAPTURE CORNER VIEWS LOOP UNTIL WE’VE GOT n_boards
	// SUCCESSFUL CAPTURES (ALL CORNERS ON THE BOARD ARE FOUND)
	//
	while(successes < n_boards) {
		IplImage *image = cvLoadImage((path+(std::to_string(successes))+".png").c_str(), CV_LOAD_IMAGE_COLOR);
		assert(image);
		IplImage *gray_image = cvCreateImage(cvGetSize(image),8,1);//subpixel
		//Skip every board_dt frames to allow user to move chessboard
		//Find chessboard corners:
		int found = cvFindChessboardCorners(
			image, board_sz, corners, &corner_count,
			CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FILTER_QUADS
		);
	
		//Get Subpixel accuracy on those corners
		cvCvtColor(image, gray_image, CV_BGR2GRAY);
		cvFindCornerSubPix(gray_image, corners, corner_count,
						cvSize(11,11),cvSize(-1,-1), cvTermCriteria(CV_TERMCRIT_EPS+CV_TERMCRIT_ITER, 30, 0.1 ));
		//cout << corners[0].x << " " << corners[0].y << endl;
		//Draw it
		
		cvDrawChessboardCorners(image, board_sz, corners, corner_count, found);
		cvShowImage("Calibration", image );

		cvWaitKey(300);
		// If we got a good board, add it to our data
		if( corner_count == board_n ) {
			step = successes*board_n;
			for( int i=step, j=0; j<board_n; ++i,++j ) {
				CV_MAT_ELEM(*image_points, float,i,0) = corners[j].x;
				CV_MAT_ELEM(*image_points, float,i,1) = corners[j].y;
				CV_MAT_ELEM(*object_points,float,i,0) = j/board_w*3;
				CV_MAT_ELEM(*object_points,float,i,1) = j%board_w*3;
				CV_MAT_ELEM(*object_points,float,i,2) = 0.0f;
				//printf("%d %d\n",j/board_w*3,j%board_w*3);
			}
			CV_MAT_ELEM(*point_counts, int,successes,0) = board_n;
			successes++;
		}
	
	} //END COLLECTION WHILE LOOP.*/
	
	// At this point we have all of the chessboard corners we need.
	// Initialize the intrinsic matrix such that the two focal
	// lengths have a ratio of 1.0
	//
	CV_MAT_ELEM( *intrinsic_matrix, float, 0, 0 ) = 1.0f;
	CV_MAT_ELEM( *intrinsic_matrix, float, 1, 1 ) = 1.0f;

	//CALIBRATE THE CAMERA!
	cvCalibrateCamera2(
		object_points, image_points,
		point_counts, cvSize(640, 480),
		intrinsic_matrix, distortion_coeffs,
		NULL, NULL,0 //CV_CALIB_FIX_ASPECT_RATIO
	);
	
	// SAVE THE INTRINSICS AND DISTORTIONS
	cvSave("Intrinsics.xml",intrinsic_matrix);
	cvSave("Distortion.xml",distortion_coeffs);

	// EXAMPLE OF LOADING THESE MATRICES BACK IN:
	CvMat *intrinsic = (CvMat*)cvLoad("Intrinsics.xml");
	CvMat *distortion = (CvMat*)cvLoad("Distortion.xml");

	// Build the undistort map that we will use for all
	// subsequent frames.
	//
	IplImage* mapx = cvCreateImage(cvSize(640, 480), IPL_DEPTH_32F, 1);
	IplImage* mapy = cvCreateImage(cvSize(640, 480), IPL_DEPTH_32F, 1);
	cvInitUndistortMap(intrinsic, distortion, mapx, mapy);

	// Just run the camera to the screen, now showing the raw and
	// the undistorted image.
	//
	cvNamedWindow("Undistort");
	IplImage *image = cvQueryFrame( capture );
	while(image) {
		IplImage *t = cvCloneImage(image);
		cvShowImage("Calibration", image ); // Show raw image
		cvRemap( t, image, mapx, mapy );
		// Undistort image
		cvReleaseImage(&t);
		cvShowImage("Undistort", image);
		// Show corrected image
		//Handle pause/unpause and ESC
		int c = cvWaitKey(15);
		if(c =='p') {
			c = 0;
			while(c != 'p' && c != 27) {
				c = cvWaitKey(250);
			}
		}
		if(c == 27)
			break;
		image = cvQueryFrame( capture );
	}
	return 0;
}

