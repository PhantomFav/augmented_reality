#include <opencv2/opencv.hpp>
#include <stdio.h>
#include <iostream>
#include <string>

using namespace cv;
using namespace std;

int main(int, char**)
{
    VideoCapture cap1(0); // open the default camera
    if(!cap1.isOpened())  // check if we succeeded
        return -1;

	cvNamedWindow("Capture");

	char buffer [20]={0};
	int count=0;
	int key=0;
	int stop=0;
    for(;;){
        Mat frame;
        cap1 >> frame; // get a new frame from camera

        // do any processing
        imshow("Capture", frame );
        key=waitKey(50);
        
        switch(key){   // you can increase delay to 2 seconds here
        case 1048673:
        	sprintf(buffer,"../data/imagel%d.png",count);
        	imwrite(buffer, frame);
        	count++;
        	break;
        case 1048608:   // you can increase delay to 2 seconds here
        	stop=1;
        	break;
        }
        
        if(stop==1) break;
        frame.release();

    }
    // the camera will be deinitialized automatically in VideoCapture destructor
    return 0;
}
