#include <math.h>
#include <GL/glut.h>
#include <opencv2/opencv.hpp>
#include <iostream>
#include <stdio.h>

//custom
#include "CameraCalibration.hpp"
#include "MarkerDetector.hpp"

using namespace cv;
using namespace std;
VideoCapture webcam(1);

GLuint listIndex;
GLuint texture;

Mat image;
Mat rvect;
Mat tvect;
Size patternsize(6,4);
Mat edges;
CvMat *in = (CvMat*)cvLoad("../Intrinsics.xml");
Mat intrinsic(in);
CvMat *dis = (CvMat*)cvLoad("../Distortion.xml");
Mat distortion(dis);

GLint width = 640;
GLint heigth = 480;
GLdouble fx = intrinsic.at<float>(0,0);
GLdouble fy = intrinsic.at<float>(1,1);
GLdouble cx = intrinsic.at<float>(0,2);
GLdouble cy = intrinsic.at<float>(1,2);
GLdouble near = 0.01;
GLdouble far = 100.0;

MarkerDetector marker_detector;
vector<Transformation> transformations;

GLuint ConvertMatToTexture(Mat image)
{
	GLuint texture;
	glGenTextures(1, &texture);		// Create The Texture
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S , GL_REPEAT);
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glTexImage2D(GL_TEXTURE_2D, 0, 3, image.cols, image.rows, 0, GL_BGR, GL_UNSIGNED_BYTE, image.data);

	return texture;
}

void drawBackground(Mat frame)
{
    const GLfloat squareVertices[] =
	{
		(float)width, (float)heigth,
		0, (float)heigth,
		(float)width, 0,
		0, 0
	};

	static const GLfloat textureVertices[] =
	{
		0, 1,
		0, 0,
		1, 1,
		1, 0
	};

	static const GLfloat proj[] =
	{
		0, -2.f/((float)width), 0, 0,
		-2.f/((float)heigth), 0, 0, 0,
		0, 0, 1, 0,
		1, 1, 0, 1
	};


	texture = ConvertMatToTexture(frame);

    glMatrixMode(GL_PROJECTION);
    glLoadMatrixf(proj);
    
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    glDepthMask(GL_FALSE);
    glDisable(GL_COLOR_MATERIAL);
    
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture);
    
    // Update attribute values.
    glVertexPointer(2, GL_FLOAT, 0, squareVertices);
    glEnableClientState(GL_VERTEX_ARRAY);
    glTexCoordPointer(2, GL_FLOAT, 0, textureVertices);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    
    glColor4f(1,1,1,1);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    
    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glDisable(GL_TEXTURE_2D);
}

GLvoid DrawVideo(){
	Mat frame;
	Mat operation;
	Mat rotm;
	GLfloat dirMat[16];

	webcam >> frame; // get a new frame from camera

	drawBackground(frame);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	//GLdouble proj[16]={2*fx/width,0,0,0,0,-2*fy/heigth,0,0,2*(cx/width)-1,1-2*(cy/heigth),(far+near)/(far-near),1,0,0,2*far*near/(near-far),0};
	//GLdouble proj[16]={2.0*fx/width,0.0,0.0,0.0,0.0,2.0*fy/heigth,0,0,2.0*(cx/width)-1.0,2.0*(cy/heigth)-1.0,-(far+near)/(far-near),-1.0,0.0,0.0,-2.0*far*near/(far-near),0.0};
	GLdouble proj[16]={-2.0*fx/width,0.0,0.0,0.0,0.0,2.0*fy/heigth,0,0,2.0*(cx/width)-1.0,2.0*(cy/heigth)-1.0,-(far+near)/(far-near),-1.0,0.0,0.0,-2.0*far*near/(far-near),0.0};

	glLoadMatrixd(proj);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	marker_detector.processFrame(frame);

	transformations = marker_detector.getTransformations();

    
    glDepthMask(GL_TRUE);
    glEnable(GL_DEPTH_TEST);
    //glDepthFunc(GL_LESS);
    //glDepthFunc(GL_GREATER);
    
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);
	    glPushMatrix();
		    glLineWidth(3.0f);

			float lineX[] = {0,0,0,1,0,0};
		    float lineY[] = {0,0,0,0,1,0};
		    float lineZ[] = {0,0,0,0,0,1};
		    
		    const GLfloat squareVertices[] = {
		        -0.5f,  0.5f,
		        0.5f,   0.5f,
		        -0.5f, -0.5f,
		        0.5f,  -0.5f,
		    };
		    const GLubyte squareColors[] = {
		        255, 255,   0, 255,
		        0,   255, 255, 255,
		        0,     0,   0,   0,
		        255,   0, 255, 255,
		    };

			if(!transformations.empty()){

				Matrix44 glMatrix = transformations[0].getMat44();
				Vector3 translation = Vector3();
				translation.data[0] = glMatrix.mat[3][0];
				translation.data[1] = glMatrix.mat[3][1];
				translation.data[2] = glMatrix.mat[3][2];

				glLoadIdentity();
				glRotatef(-3.5, 0.0, 1.0, 0.0);
				glMultMatrixf(reinterpret_cast<const GLfloat*>(&glMatrix.data[0]));
				
				glPushMatrix();
					glColor4f(0.0f, 0.0f, 0.0f, 1.0f);
					glTranslatef(0.0, 0.0, 0.2);
					glRotatef(90.0, 1.0, 0.0, 0.0);
					glScalef(0.2, 0.2, 0.2);
					glutWireIcosahedron();
					//glutWireTeapot(0.5);
				glPopMatrix();

				// draw data
				/*glVertexPointer(2, GL_FLOAT, 0, squareVertices);
				glEnableClientState(GL_VERTEX_ARRAY);
				glColorPointer(4, GL_UNSIGNED_BYTE, 0, squareColors);
				glEnableClientState(GL_COLOR_ARRAY);

				glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
				glDisableClientState(GL_COLOR_ARRAY);*/

				//float scale = 0.5;
				//glScalef(scale, scale, scale);

				glColor4f(1.0f, 0.0f, 0.0f, 1.0f);
				glVertexPointer(3, GL_FLOAT, 0, lineX);
				glDrawArrays(GL_LINES, 0, 2);

				glColor4f(0.0f, 1.0f, 0.0f, 1.0f);
				glVertexPointer(3, GL_FLOAT, 0, lineY);
				glDrawArrays(GL_LINES, 0, 2);

				glColor4f(0.0f, 0.0f, 1.0f, 1.0f);
				glVertexPointer(3, GL_FLOAT, 0, lineZ);
				glDrawArrays(GL_LINES, 0, 2);


				glColor4f(1.0, 1.0, 1.0, 0.0);

		    }
	    glPopMatrix();
    glDisableClientState(GL_VERTEX_ARRAY);
}


void init(){
	glEnable(GL_DEPTH_TEST);
	glClearColor(0.0, 0.0, 0.0, 1.0);
}


void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	DrawVideo();
	glutSwapBuffers();
}

//http://www.songho.ca/opengl/gl_projectionmatrix.html
void reshape(int w, int h){

	glViewport(0, 0, (GLsizei)w, (GLsizei)h);

}


void keyboard (unsigned char key, int x, int y)
{
	switch(key){
		case 27: case 'q':
		exit (0);
		break;
	}
}


/*void initialize_opencv(){
	listIndex = glGenLists(1);
	glNewList(listIndex, GL_COMPILE);
		glPushMatrix();
			glTranslatef((GLfloat)width/2-cx,cy-(GLfloat)heigth/2, -682.0);
			glBegin(GL_QUADS);
				glTexCoord2f (0.0, 1.0);
				glVertex3f(-320.0f, -240.0f, 0.0f);
				glTexCoord2f (1.0, 1.0);
				glVertex3f(320.0f,-240.0f, 0.0f);
				glTexCoord2f (1.0, 0.0);
				glVertex3f(320.0f,240.0f, 0.0f);
				glTexCoord2f (0.0, 0.0);
				glVertex3f(-320.0f, 240.0f, 0.0f);
			glEnd();
		glPopMatrix();
	glEndList();
}
*/

int main(int argc, char **argv)
{

	float* dist = (float*)distortion.data;
	CameraCalibration calibration = CameraCalibration(intrinsic.at<float>(0), intrinsic.at<float>(4),
														intrinsic.at<float>(2), intrinsic.at<float>(5), dist);
	marker_detector = MarkerDetector(calibration);

	glutInit(&argc, argv);
	glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
    
	glutInitWindowSize(width, heigth); // 4/3
    glutInitWindowPosition(100, 100);
    glutCreateWindow("Cam");
	
    if(!webcam.isOpened())  // check if we succeeded
    	return -1;

    //glEnable(GL_CULL_FACE);
    //initialize_opencv();
    init();
    glutDisplayFunc(display);
    glutKeyboardFunc(keyboard);
    glutIdleFunc(display);
    glutReshapeFunc(reshape);
    glutMainLoop();
}