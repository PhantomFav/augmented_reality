# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/luca/Opencv/marker_omography/src/CameraCalibration.cpp" "/home/luca/Opencv/marker_omography/release/CMakeFiles/binary.dir/src/CameraCalibration.cpp.o"
  "/home/luca/Opencv/marker_omography/src/GeometryTypes.cpp" "/home/luca/Opencv/marker_omography/release/CMakeFiles/binary.dir/src/GeometryTypes.cpp.o"
  "/home/luca/Opencv/marker_omography/src/Marker.cpp" "/home/luca/Opencv/marker_omography/release/CMakeFiles/binary.dir/src/Marker.cpp.o"
  "/home/luca/Opencv/marker_omography/src/MarkerDetector.cpp" "/home/luca/Opencv/marker_omography/release/CMakeFiles/binary.dir/src/MarkerDetector.cpp.o"
  "/home/luca/Opencv/marker_omography/src/TinyLA.cpp" "/home/luca/Opencv/marker_omography/release/CMakeFiles/binary.dir/src/TinyLA.cpp.o"
  "/home/luca/Opencv/marker_omography/src/main.cpp" "/home/luca/Opencv/marker_omography/release/CMakeFiles/binary.dir/src/main.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/opencv"
  "../$(CMAKE_CURRENT_SOURCE_DIR)/include"
  "../$(CMAKE_CURRENT_SOURCE_DIR)/src"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
